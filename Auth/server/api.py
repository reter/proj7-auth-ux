import flask
from flask import Flask, redirect, url_for, request, render_template, flash, session
from flask_restful import Resource, Api
import arrow  
import acp_times
import config
import logging
import pymongo
import os
import time
from pymongo import MongoClient
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
from passlib.apps import custom_app_context as pwd_context
import random
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
from flask_wtf.csrf import CSRFProtect
from urllib.parse import urlparse, urljoin
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo





app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
csrf = CSRFProtect(app)
client = MongoClient('mongodb://mongodb:27017/')
db = client.tododb
DEFAULT = 20 
users = db.usersdb
ITEMS = {}




@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return flask.render_template('404.html'), 404



@app.route("/_calc_times")
def _calc_times():

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999,type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    distance = request.args.get('distance',999, type=float)
    begin_date = request.args.get('bd', 999, type=str)
    begin_time = request.args.get('bt', 999, type=str)
    row = request.args.get("row")
    timeformat = "{}T{}".format(begin_date, begin_time)
    timing = arrow.get(timeformat)
    open_time = acp_times.open_time(km, distance, timing.isoformat())
    close_time = acp_times.close_time(km, distance, timing.isoformat())
    result = {"open": open_time, "close": close_time}
    ITEMS[row] = [km, open_time, close_time]  # for submit

    return flask.jsonify(result=result)


@app.route("/submit")
def _submit():

    if len(ITEMS) == 0:
        result = {"successful": False, "err": "nothing"}
        return flask.jsonify(result=result)
            
        db.tododb.delete_many({})
    if isinstance(ITEMS, str)== 1:
        result = {"successful": False, "err": "nothing"}
        return flask.jsonify(result=result)

        db.tododb.delete_many({})
    else:
        for i in ITEMS.values():
            item_doc = {
               
                'km': i[0],
                'open_time': i[1],
                'close_time': i[2]
            }
            db.tododb.insert_one(item_doc)
        result = {"successful": True}
        return flask.jsonify(result=result)

@app.route("/display")
@login_required
def _display():
    ITEMS.clear()
    _items = db.tododb.find()
    items = [item for item in _items]

    for item in items:
        item['open_time'] = arrow.get(item['open_time']).format('dddd D/M HH:mm')
        item['close_time'] = arrow.get(item['close_time']).format('dddd D/M HH:mm')

    return render_template('display.html', items=items)

#CSV , JSON
class ListAll(Resource):
    def get(self):


        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')


        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT
        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items],
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(ListAll, '/listAll')
class ListAllJSON(Resource):
    def get(self):

        #copy from listall
        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
       
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items],
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(ListAllJSON, '/listAll/json')

class ListAllCSV(Resource):
    def get(self):
       
        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
 
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        opencsv =""
        closecsv=""
        for item in items:
            opencsv += arrow.get(item['open_time']).format('dddd D/M HH:mm') + ','
            closecsv+= arrow.get(item['close_time']).format('dddd D/M HH:mm') + ','
        return (opencsv+closecsv)
api.add_resource(ListAllCSV, '/listAll/csv')

class OpenOnly(Resource):
    def get(self):

 
        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
 
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(OpenOnly, '/listOpenOnly')
class OpenJSONOnly(Resource):
    def get(self):

        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'open_time': [arrow.get(item['open_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(OpenJSONOnly, '/listOpenOnly/json')

class OpenCSVOnly(Resource):
    def get(self):

        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
    
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("open_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        open_csv = ""
        for item in items:
            open_csv += arrow.get(item['open_time']).format('dddd D/M HH:mm') + ','
        return open_csv
api.add_resource(OpenCSVOnly, '/listOpenOnly/csv')
class CloseOnly(Resource):
    def get(self):

        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
 
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'closetime': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(CloseOnly, '/listCloseOnly')
class CloseJSONOnly(Resource):
    def get(self):
        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
        
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth fail",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        return {
            'close_time': [arrow.get(item['close_time']).format('dddd D/M HH:mm') for item in items]
        }
api.add_resource(CloseJSONOnly, '/listCloseOnly/json')
class CloseCSVOnly(Resource):
    def get(self):

 
        if "token" in session:
            if session['token'] == None:
                token = request.args.get('token')
            else:
                token = session['token']
        else:
            token = request.args.get('token')
            
 
        if token == None:
            return "No token",401
        elif verify_auth_token(token) == None:
            return "auth failed",401

        top = request.args.get("top")

        if(top==None):
            top = DEFAULT

        _items = db.tododb.find().sort("close_time", pymongo.ASCENDING).limit(int(top))
        items = [item for item in _items]

        close_csv = ""
        for item in items:
            close_csv += arrow.get(item['close_time']).format('dddd D/M HH:mm') + ','
        return close_csv
api.add_resource(CloseCSVOnly, '/listCloseOnly/csv')






class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('log in')

class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm = PasswordField('check Password', validators=[DataRequired(), EqualTo('password', message='Password should same')])
    submit = SubmitField('register')

def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)



@app.route('/api/test', methods=['GET', 'POST'])
def test():
    _users = db.usersdb.find()
    users = [user for user in _users]
    result = {}
    for user in users:
        flash(user['username'])
    return flask.jsonify(result=result)


@app.route('/api/users/<username>')
def getuser(username):

    user = db.usersdb.find({'username': username})
    if not user:
        flask.abort(400)
    return flask.jsonify({'username': user['username'],'id':user['id']})


def indexid():

    return random.randint(1, 9999)

@app.route('/api/login', methods=['GET', 'POST'])
def login():

    form = LoginForm()
    if form.validate_on_submit():
        user = users.find_one({"username": form.username.data})
        if user == None:
            flash("need a username.")
            return redirect(url_for('register'))


        password = user['password']
        if not verify_password(form.password.data, password):
            flash("Password wrong")
            return redirect(url_for('login'))

        uid = User(user['id'])
        session['id'] = user['id']
        if login_user(uid, remember=form.remember_me.data):
            flash("logininggggg")
 
        else:
            flash("login fail, so sad")

        next = request.args.get("next")
        if not is_safe_url(next):
            return flask.abort(400)
        if next:
            return redirect(next)
        return redirect('/')

    return render_template('login.html',  title='log in', form=form)

@app.route('/api/register', methods=['GET', 'POST'])
def register():

    form = RegisterForm()
    if form.validate_on_submit():
       
        indid = indexid()
        hashpwd = hash_password(form.password.data)

        usersd = {
            "id": indid,
            "username": form.username.data,
            "password": hashpwd
        }
        users.insert_one(usersd)

        rua = {
            "index": indid,
            "username": form.username.data,
            "password": hashpwd
        }
        session['token'] = None

        return flask.jsonify({'index': url_for('getuser', username=form.username.data, _external=True), 'id': indid, 'username': form.username.data, 'password': hashpwd})

    return render_template('register.html',  title='register', form=form)




class User(UserMixin):

    def __init__(self, id, active=True):
        self.id = id 
        self.active = active
 

    def is_active(self):
         return self.active


    def get_id(self):
        return self.id

login_manager = LoginManager()
login_manager.setup_app(app)
login_manager.session_protection = "strong"
login_manager.login_view = u"login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = u"reauth"

@login_manager.user_loader
def load_user(ID):
   
    user = users.find_one({"id": ID})
    if user != None: return User(ID)
    return None


@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("404.html")  

@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash("Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    session['token'] = None
    return redirect(url_for("index"))

def generate_auth_token(id, expiration=600):
    s = Serializer(app.secret_key, expires_in=expiration)
    token = s.dumps({'id': id})
    return {'token': token, 'duration': expiration}

def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    
    except BadSignature:
        return None    
    return "Success"


@app.route("/api/token")
@login_required
def token():
    """
 this will show token page
    """

    try:
        id = session.get('id')

        tokeninfo = generate_auth_token(id, 600)
        tokentransfer = tokeninfo['token'].decode('utf-8')
        result = {'token':tokentransfer, 'duration': 60}
        session['token'] =tokentransfer
        return flask.jsonify(result=result)
        
    except:
        return "Unauthorized",401


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(port=CONFIG.PORT, host="0.0.0.0")
    csrf.init_app(app)
