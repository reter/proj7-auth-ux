"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from math import ceil


#number's meaning of odd: when brevet distance equal to control distance:minutes, for example,200 be 13*60+30 =810minutes
# Minimum times as [(from_dist, to_dist, speed),(from_dist, to_dist, speed), ... ]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    max_speed = [(0, 200, 34), (200, 400, 32), (400, 600, 30),(600, 1000, 28), (1000, 1300, 28)]

    start_time = arrow.get(brevet_start_time)
    update_hours = 0
    distance_left = control_dist_km
    for from_dist, to_dist, speed in max_speed:
        seg_length = to_dist - from_dist
        if distance_left > seg_length:
            update_hours += seg_length / speed
            distance_left -= seg_length
        else:
            update_hours += distance_left / speed
            open_time = start_time.replace(hour=ceil(update_hours))
            return open_time.isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    min_speed = [(0, 200, 15), (200, 400, 15), (400, 600, 15),(600, 1000, 11.428), (1000, 1300, 13.333)]

    final_close = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}

    start_time = arrow.get(brevet_start_time)
    if control_dist_km >= brevet_dist_km:
        duration = final_close[brevet_dist_km]
        finish_time = start_time.replace(hour=ceil(duration))
        return finish_time.isoformat()
    update_hour = 0
    distance_left = control_dist_km
    for from_dist, to_dist, speed in min_speed:
        seg_length = to_dist - from_dist
        if distance_left > seg_length:
            update_hour += seg_length / speed
            distance_left -= seg_length
        else:
            update_hour += distance_left / speed
            cut_time = start_time.replace(hour=ceil(update_hour))
            return cut_time.isoformat()

    return arrow.now().isoformat()
